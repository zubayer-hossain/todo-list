<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TodoRequest;
use App\Models\Role;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TodoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TodoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Todo::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/todo');
        CRUD::setEntityNameStrings('todo', 'todos');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        if (backpack_user()->hasPermissionTo('todo list')) {
            CRUD::addColumn([
                'name' => 'id',
            ]);
            CRUD::addColumn([
                'name' => 'title',
            ]);
            CRUD::addColumn([
                'name' => 'description',
            ]);

//            if (backpack_auth()->user()->id === 1) {
//                CRUD::addColumn([
//                    'name'     => 'role_id',
//                    'label'    => "Assigned to",
//                    'type'     => 'closure',
//                    'function' => function ($entry) {
//                        return Role::query()->find($entry->role_id)->name;
//                    }
//                ]);
//            }

            if (!backpack_user()->hasPermissionTo('todo create')) {
                $this->crud->removeButton('create');
            }

            if (!backpack_user()->hasPermissionTo('todo show')) {
                $this->crud->removeButton('show');
            }

            if (!backpack_user()->hasPermissionTo('todo edit')) {
                $this->crud->removeButton('update');
            }

            if (!backpack_user()->hasPermissionTo('todo delete')) {
                $this->crud->removeButton('delete');
            }

            if (backpack_auth()->user()->id !== 1) {
                $this->crud->addClause('where', 'role_id', '=', backpack_user()->id);
            }
        } else {
            abort('403');
        }
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        if (backpack_user()->hasPermissionTo('todo create')) {
            CRUD::setValidation(TodoRequest::class);
            CRUD::addField([
                'name' => 'title',
                'type' => 'text'
            ]);
            CRUD::addField([
                'name' => 'description',
                'type' => 'textarea'
            ]);

            CRUD::addField([
                'name'    => 'role_id',
                'label'   => "Assign to User",
                'type'    => 'select2',
                'entity'  => 'users', // the method that defines the relationship in your Model
                'model'   => "App\Models\User", // foreign key model
                'options' => (function ($query) {
                    return $query->where('id', '!=', 1)->get();
                }),
            ]);

        } else {
            abort('403');
        }

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        if (backpack_user()->hasPermissionTo('todo edit')) {
            $current_entry = $this->crud->getCurrentEntry();
            if (backpack_user()->id !== 1 && $current_entry->role_id !== backpack_user()->id) {
                abort(403);
            }

            $this->setupCreateOperation();
        } else {
            abort('403');
        }
    }

    protected function setupShowOperation()
    {
        if (backpack_user()->hasPermissionTo('todo show')) {
            $this->crud->set('show.setFromDb', false);

            $current_entry = $this->crud->getCurrentEntry();
            if (backpack_user()->id !== 1 && $current_entry->role_id !== backpack_user()->id) {
                abort(403);
            }

            CRUD::addColumn([
                'name' => 'id',
            ]);
            CRUD::addColumn([
                'name' => 'title',
            ]);
            CRUD::addColumn([
                'name' => 'description',
            ]);

            if (backpack_auth()->user()->id === 1) {
                CRUD::addColumn([
                    'name'     => 'role_id',
                    'label'    => "Assigned to",
                    'type'     => 'closure',
                    'function' => function ($entry) {
                        return User::query()->find($entry->role_id)->name;
                    }
                ]);
            }

            if (!backpack_user()->hasPermissionTo('todo edit')) {
                $this->crud->removeButton('update');
            }

            if (!backpack_user()->hasPermissionTo('todo delete')) {
                $this->crud->removeButton('delete');
            }
        }
    }
}
