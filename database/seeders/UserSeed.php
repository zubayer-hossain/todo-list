<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeed extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::query()->create(
            [
                'name'              => 'Super Admin',
                'email'             => 'superadmin@gmail.com',
                'email_verified_at' => now(),
                'is_admin'          => 1,
                'password'          => Hash::make('password'),
                'remember_token'    => Str::random(10),
            ]
        );

        User::query()->create(
            [
                'name'              => 'Mr User 1',
                'email'             => 'user1@gmail.com',
                'email_verified_at' => now(),
                'is_admin'          => 0,
                'password'          => Hash::make('password'),
                'remember_token'    => Str::random(10),
            ]
        );

        User::query()->create(
            [
                'name'              => 'Mr Moderator',
                'email'             => 'moderator1@gmail.com',
                'email_verified_at' => now(),
                'is_admin'          => 0,
                'password'          => Hash::make('password'),
                'remember_token'    => Str::random(10),
            ]
        );
    }
}
